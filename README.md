# Dexter

A source code forensic analysis tool based on the works of [Adam Tornhill](https://www.adamtornhill.com) [_Your code as a Crime Scene_](https://pragprog.com/book/atcrime/your-code-as-a-crime-scene) and [_Software Design X-Rays_](https://pragprog.com/book/atevol/software-design-x-rays).

## WARNING

Code without automated tests. Use at your own risk.

## Installing from source

### Requirements

* Git 1.9 or greater
* Go 1.8 or up

### Steps

* Grab the code with `go get -u github.com/svera/dexter`
* From source folder, run `dep ensure` to retrieve dependencies and then `go install cmd`

## Usage

Currently, Dexter features the following set of analysis:

### Hotspots

Returns the structure of a repository as a tree, with each file with information of its size in lines of code and the number of 
revisions done to it. This can be used to identify which files are more in need to be refactored, using a visualization like packed circles.

To use it: `dexter hotspots <repository location>`

It is recommended to limit the scope of reviewed commits with `--since` parameter, as the process can take a long time to complete.
If you want to use resulting data with the [D3](https://d3js.org) javascript framework, use `--format d3`. Otherwise, results will be returned
as a plain JSON.

### Complexity trend

Returns a series of snapshots of a file in a specific moment, each of them with information of its size in lines of code and 
calculated complexity, measured in nesting depth. Comparing these snapshots, we can see the evolution in complexity of the specified file.

To use it: `dexter trend <repository location> <file path relative to repository>`

### Change coupling

Return a list of files which have a certain degree of coupling. Two or more files are considered as coupled if 
they change together in at least a certain number of commits and 2) the files are changed together in at least 
a minimum percent of the total commits done to either file.

To use it: `dexter coupling <repository location>`

Again, limit the scope of reviewed commits with `--since` parameter, as the process can take a long time to complete otherwise.

## Examples

The [examples](/examples) folder contains some examples of visualizations that use data collected by Dexter. You can use a web server (like the one coming with PHP) to run them. To do so, run `php -S localhost:3000` from the `examples` folder and point your browser to either `localhost:3000\hotspots.html` or `localhost:3000\trend.html`.
package helpers

import (
	"fmt"
	"io"

	"github.com/svera/gocloc"
	enry "gopkg.in/src-d/enry.v1"
)

var clocOpts *gocloc.ClocOptions
var definedLanguages *gocloc.DefinedLanguages

func init() {
	definedLanguages = gocloc.NewDefinedLanguages()
	clocOpts = gocloc.NewClocOptions()
	clocOpts.Debug = false
}

func Loc(filePath string, fileContents io.Reader) (*gocloc.ClocFile, error) {
	langs := enry.GetLanguagesByExtension(filePath, []byte{}, []string{})
	for _, l := range langs {
		if _, ok := definedLanguages.Langs[l]; ok {
			return gocloc.AnalyzeReader(filePath, definedLanguages.Langs[l], fileContents, clocOpts), nil
		}
	}
	return &gocloc.ClocFile{}, fmt.Errorf("Unsupported language: %s\n", filePath)
}

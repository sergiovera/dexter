package measurements

import (
	"math"
	"sort"

	"gitlab.com/sergiovera/dexter/pkg/vcs"
)

// Pair stores coupling information of two files (Number of times they are commited together
// and percentage of coupling related with total number of commits of either file)
type Pair struct {
	Occurences int
	Coupling   float64
}

type CouplingStruct map[string]map[string]*Pair

// Coupling returns a slice of file pairs, the number of times they are commited together and their degree of coupling.
func Coupling(repo vcs.VCS, revisions []string, minimumRevisions int, minimumCoupling float64, progress chan<- struct{}) (CouplingStruct, error) {
	defer close(progress)
	pairs := CouplingStruct{}
	occurences := map[string]int{}

	//revisions = []string{"868cf28cac6dca1c093152be3ec0deee2ddaebcf"}
	for _, rev := range revisions {
		//log.Println(rev)
		if err := calculatePairs(repo, rev, pairs, occurences); err != nil {
			return pairs, err
		}
		progress <- struct{}{}
	}
	applyThresholds(pairs, occurences, minimumRevisions, minimumCoupling)
	return pairs, nil
}

func calculatePairs(repo vcs.VCS, rev string, coupling CouplingStruct, occurences map[string]int) error {
	files, err := repo.FilesChanged(rev)
	sort.Strings(files)
	//log.Printf("rev %s, %d files\n", rev, len(files))
	if err != nil {
		return err
	}

	if len(files) == 1 {
		occurences[files[0]]++
		return nil
	}

	// the number of needed iterations can be calculated with a binomial coefficient
	for i := 0; i < len(files)-1; i++ {
		for j := i + 1; j < len(files); j++ {
			file1 := files[i]
			file2 := files[j]

			occurences[file1]++
			occurences[file2]++

			if mp, ok := coupling[file1]; ok {
				if pair, ok2 := mp[file2]; ok2 {
					pair.Occurences++
				} else {
					coupling[file1][file2] = &Pair{1, 0}
				}
			} else {
				coupling[file1] = map[string]*Pair{
					file2: &Pair{1, 0},
				}
			}
		}
	}
	return nil
}

// According to Adam Tornhill's "Software Design X-Rays":
// "Two or more files are coupled in time if 1) they change together in at least <minimumRevisions> commits and 2) the files are changed
// together in at least <minimumCoupling> percent of the total commits done to either file"
func applyThresholds(coupling CouplingStruct, occurences map[string]int, minimumRevisions int, minimumCoupling float64) {
	for file1, mp := range coupling {
		for file2 := range mp {
			p := mp[file2]
			if p.Occurences < minimumRevisions {
				delete(mp, file2)
			} else {
				file1Percentage := (float64(p.Occurences) / float64(occurences[file1])) * 100
				file2Percentage := (float64(p.Occurences) / float64(occurences[file2])) * 100
				if file1Percentage > minimumCoupling || file2Percentage > minimumCoupling {
					p.Coupling = math.Max(file1Percentage, file2Percentage)
				} else {
					delete(mp, file2)
				}
			}
		}
		if len(coupling[file1]) == 0 {
			delete(coupling, file1)
		}
	}
}

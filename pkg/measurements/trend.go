package measurements

import (
	"fmt"
	"time"

	"gitlab.com/sergiovera/dexter/pkg/helpers"
	"gitlab.com/sergiovera/dexter/pkg/vcs"
)

// Snapshot stores file metadata such as number of lines of code, comments and complexity value at a specific time
type Snapshot struct {
	Datetime   time.Time `json:"datetime"`
	Code       int32     `json:"code"`
	Comments   int32     `json:"comments"`
	Complexity float32   `json:"complexity"`
}

// Trend is a slice of snapshots that can be sorted by datetime
type Trend []Snapshot

func (t Trend) Len() int {
	return len(t)
}

func (t Trend) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t Trend) Less(i, j int) bool {
	return t[i].Datetime.Before(t[j].Datetime)
}

// GetTrend returns the complexity trend of a file since a specific time
func GetTrend(repo vcs.VCS, hashes map[time.Time]string, filePath string, progress chan<- struct{}) (Trend, error) {
	trend := []Snapshot{}
	defer close(progress)
	for dateTime, hash := range hashes {
		fileContents, err := repo.GetFileContentsAtRevision(filePath, hash)
		if err != nil {
			return trend, fmt.Errorf("Couldn't get content of %s at revision %s: %s", filePath, hash, err)
		}
		loc, err := helpers.Loc(filePath, fileContents)
		if err != nil {
			continue
		}
		snap := Snapshot{
			dateTime,
			loc.Code,
			loc.Comments,
			loc.Complexity,
		}
		trend = append(trend, snap)
		progress <- struct{}{}
	}

	return trend, nil
}

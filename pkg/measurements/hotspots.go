package measurements

import (
	"log"
	"os"

	"gitlab.com/sergiovera/dexter/pkg/helpers"
	"gitlab.com/sergiovera/dexter/pkg/vcs"
)

// Hotspot is a struct that stores lines of code of a file and number of revisions of it
type Hotspot struct {
	LOC       int `json:"loc"`
	Revisions int `json:"revisions"`
}

// Revisions counts, given a slice of commit hashes, the number of times files were included in those commits
func Revisions(repo vcs.VCS, hashes []string, progress chan<- struct{}) map[string]interface{} {
	defer close(progress)
	res := make(map[string]interface{})
	for _, hash := range hashes {
		fileNames, _ := repo.FilesChanged(hash)
		for _, name := range fileNames {
			if _, err := os.Stat(repo.Path() + "/" + name); os.IsNotExist(err) {
				delete(res, name)
				continue
			}
			if _, ok := res[name]; !ok {
				res[name] = &Hotspot{}
			}
			res[name].(*Hotspot).Revisions++
		}
		progress <- struct{}{}
	}
	return res
}

// Hotspots returns a map of files included in revisions since "limit", with its calculated hotspot values
func Hotspots(repo vcs.VCS, revisions *map[string]interface{}, progress chan<- struct{}) []error {
	defer close(progress)
	errors := []error{}
	for filename := range *revisions {
		filePath := repo.Path() + "/" + filename
		fp, err := os.Open(filePath)
		if err != nil {
			log.Println(err)
		}
		clocFile, err := helpers.Loc(filePath, fp)
		if err := fp.Close(); err != nil {
			errors = append(errors, err)
		}
		// Ignore files whose number of LOC couldn't be gathered
		if err != nil {
			errors = append(errors, err)
			delete(*revisions, filename)
			continue
		}
		(*revisions)[filename].(*Hotspot).LOC = int(clocFile.Code)
		progress <- struct{}{}
	}
	return errors
}

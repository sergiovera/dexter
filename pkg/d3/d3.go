// Package d3 provides methods to export data to formats suitable to be used in the D3 visualization library.
// https://d3js.org/
package d3

import (
	"encoding/json"
	"time"

	"gitlab.com/sergiovera/dexter/pkg/measurements"
)

type D3Hotspot struct {
	Name      string       `json:"name"`
	LOC       int          `json:"size,omitempty"`
	Revisions int          `json:"revisions,omitempty"`
	Children  []*D3Hotspot `json:"children,omitempty"`
}

type D3Snapshot struct {
	Datetime   time.Time `json:"datetime"`
	LOC        int       `json:"loc"`
	Comment    int       `json:"comment"`
	Complexity float64   `json:"complexity"`
}

// ToHierarchy converts a nested map of hotspots to a JSON hierarchical format
// that can be parsed by D3.
// Check https://github.com/d3/d3-hierarchy#hierarchy for more information
func ToHierarchy(in map[string]interface{}) ([]byte, error) {
	tree := &D3Hotspot{
		Name: "root",
	}
	return json.Marshal(toHierarchyChildren(in, tree))
}

func toHierarchyChildren(in map[string]interface{}, parent *D3Hotspot) *D3Hotspot {
	for name, value := range in {
		switch v := value.(type) {
		case *measurements.Hotspot:
			parent.Children = append(parent.Children, &D3Hotspot{
				Name:      name,
				LOC:       v.LOC,
				Revisions: v.Revisions,
			})
		case map[string]interface{}:
			tree := &D3Hotspot{
				Name: name,
			}
			parent.Children = append(
				parent.Children,
				toHierarchyChildren(value.(map[string]interface{}), tree))
		}
	}
	return parent
}

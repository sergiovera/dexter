package d3

import (
	"testing"

	"gitlab.com/sergiovera/dexter/pkg/measurements"
)

func TestToHierarchy(t *testing.T) {
	input := map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"x": &measurements.Hotspot{
					LOC:       5,
					Revisions: 2,
				},
				"y": &measurements.Hotspot{
					LOC:       8,
					Revisions: 3,
				},
			},
			"z": &measurements.Hotspot{
				LOC:       8,
				Revisions: 3,
			},
		},
		"j": map[string]interface{}{},
	}

	expected := `{"name":"root","children":[{"name":"a","children":[{"name":"b","children":[{"name":"x","loc":5,"revisions":2},{"name":"y","loc":8,"revisions":3}]},{"name":"z","loc":8,"revisions":3}]},{"name":"j"}]}`

	res, _ := ToHierarchy(input)
	if string(res) != expected {
		t.Errorf("Expected %s, got %s", expected, string(res))
	}
}

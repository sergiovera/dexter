package vcs

import (
	"bytes"
	"time"
)

// VCS defines the accepted interface to interact with Version Control Systems (VCS)
type VCS interface {
	Path() string
	CommitsSince(since time.Time) ([]string, error)
	FilesChanged(hash string) ([]string, error)
	RevisionsSince(filePath string, since time.Time) (map[time.Time]string, error)
	GetFileContentsAtRevision(filePath string, revision string) (*bytes.Reader, error)
}

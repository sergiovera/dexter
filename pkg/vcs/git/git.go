package git

import (
	"bytes"
	"fmt"
	"os/exec"
	"strings"
	"time"
)

type git struct {
	path string
}

func New(path string) *git {
	return &git{
		path: path,
	}
}

func (g *git) Path() string {
	return g.path
}

func (g *git) CommitsSince(since time.Time) ([]string, error) {
	hashes := []string{}
	cmd := exec.Command(
		"git",
		"-C",
		g.path,
		"log",
		fmt.Sprintf("--since=%d-%d-%d", since.Year(), since.Month(), since.Day()),
		"--pretty=format:%H")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return hashes, err
	}
	hashes = strings.Split(string(out), "\n")
	return hashes, nil
}

func (g *git) FilesChanged(hash string) ([]string, error) {
	fileNames := []string{}
	cmd := exec.Command(
		"git",
		"-C",
		g.path,
		"diff-tree",
		"--no-commit-id",
		"--name-only",
		"-r",
		hash)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fileNames, err
	}
	fileNames = strings.Split(string(out), "\n")
	// Slice last element is always empty due to the last newline
	if len(fileNames) > 0 {
		fileNames = fileNames[:len(fileNames)-1]
	}
	return fileNames, nil
}

func (g *git) RevisionsSince(filePath string, since time.Time) (map[time.Time]string, error) {
	revisions := make(map[time.Time]string)
	cmd := exec.Command(
		"git",
		"-C",
		g.path,
		"log",
		fmt.Sprintf("--since=%d-%d-%d", since.Year(), since.Month(), since.Day()),
		"--pretty=format:%H|%ci",
		// "--follow", // To take renames into account
		"--",
		filePath)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return revisions, err
	}
	raw := strings.Split(string(out), "\n")
	for _, r := range raw {
		parts := strings.Split(r, "|")
		t, err := time.Parse("2006-01-02 15:04:05 -0700", parts[1])
		if err != nil {
			return revisions, err
		}
		revisions[t] = parts[0]
	}
	if len(revisions) == 0 {
		return revisions, fmt.Errorf("%s has no revisions", filePath)
	}

	return revisions, nil
}

func (g *git) GetFileContentsAtRevision(filePath string, revision string) (*bytes.Reader, error) {
	cmd := exec.Command(
		"git",
		"-C",
		g.path,
		"show",
		revision+":"+filePath)
	fileContents, err := cmd.Output()
	if err != nil {
		return &bytes.Reader{}, err
	}
	return bytes.NewReader(fileContents), nil
}

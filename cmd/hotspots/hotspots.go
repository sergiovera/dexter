package hotspots

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/sergiovera/dexter/pkg/d3"
	"gitlab.com/sergiovera/dexter/pkg/measurements"
	"gitlab.com/sergiovera/dexter/pkg/vcs"
	"gitlab.com/sergiovera/dexter/pkg/vcs/git"

	"github.com/schollz/progressbar"
	"github.com/spf13/cobra"
	"github.com/wolfeidau/unflatten"
)

var since string
var format string
var allowedFormats map[string]struct{}

func init() {
	Cmd.Flags().StringVarP(&since, "since", "s", "1970-01-01", "Include only commits since this date")
	Cmd.Flags().StringVarP(&format, "format", "f", "json", "Output format")
	allowedFormats = map[string]struct{}{
		"json": struct{}{},
		"d3":   struct{}{},
	}
}

var Cmd = &cobra.Command{
	Use:   "hotspots [repository path]",
	Short: "Do a hotspots analysis",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		path := args[0]
		sinceTime, err := time.Parse("2006-01-02", since)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Passed date not valid, format must be YYYY-MM-DD\n")
			os.Exit(1)
		}
		if _, ok := allowedFormats[format]; !ok {
			fmt.Fprintf(os.Stderr, "Passed format '%s' not valid\n", format)
			os.Exit(1)
		}
		git := git.New(path)

		hs := revisions(git, sinceTime)
		hotspots(git, &hs)

		tree := unflatten.Unflatten(hs, func(k string) []string { return strings.Split(k, "/") })
		var formatted []byte

		switch format {
		case "d3":
			formatted, err = d3.ToHierarchy(tree)
		case "json":
			formatted, err = json.Marshal(tree)
		}

		if err != nil {
			fmt.Fprintf(os.Stderr, "Error Marshaling result: %s\n", err)
			os.Exit(1)
		} else {
			fmt.Println(string(formatted))
		}

	},
}

func revisions(repo vcs.VCS, since time.Time) map[string]interface{} {
	revs := map[string]interface{}{}
	hashes, err := repo.CommitsSince(since)
	if err != nil {
		log.Fatalf("Error obtaining commits from %s: %s", repo.Path(), err)
	}

	bar := progressbar.NewOptions(
		len(hashes),
		progressbar.OptionSetRenderBlankState(true),
		progressbar.OptionSetWriter(os.Stderr))

	progress := make(chan struct{}, len(hashes))
	go func() {
		revs = measurements.Revisions(repo, hashes, progress)
	}()

	for range progress {
		bar.Add(1)
	}
	return revs
}

func hotspots(repo vcs.VCS, revs *map[string]interface{}) {
	bar := progressbar.NewOptions(
		len(*revs),
		progressbar.OptionSetRenderBlankState(true),
		progressbar.OptionSetWriter(os.Stderr))

	progress := make(chan struct{}, len(*revs))
	go func() {
		measurements.Hotspots(repo, revs, progress)
	}()

	for range progress {
		bar.Add(1)
	}
}

package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/sergiovera/dexter/cmd/coupling"
	"gitlab.com/sergiovera/dexter/cmd/hotspots"
	"gitlab.com/sergiovera/dexter/cmd/trend"
)

func main() {
	var rootCmd = &cobra.Command{
		Use:  "dexter",
		Long: `A forensycs software anaysis tool, based on the works by Adam Tornhill`,
	}
	rootCmd.AddCommand(
		trend.Cmd,
		hotspots.Cmd,
		coupling.Cmd)

	rootCmd.Execute()
}

package trend

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"time"

	"gitlab.com/sergiovera/dexter/pkg/measurements"
	"gitlab.com/sergiovera/dexter/pkg/vcs/git"

	"github.com/schollz/progressbar"
	"github.com/spf13/cobra"
)

var since string

func init() {
	Cmd.Flags().StringVarP(&since, "since", "a", "1970-01-01", "Include only commits since this date")
}

// Cmd implements
var Cmd = &cobra.Command{
	Use:   "trend [repository path] [relative file path]",
	Short: "Do a file complexity trend analysis",
	Args:  cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		path := args[0]
		filePath := args[1]
		git := git.New(path)
		sinceTime, err := time.Parse("2006-01-02", since)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Passed date not valid, format must be YYYY-MM-DD\n")
			os.Exit(1)
		}
		hashes, err := git.RevisionsSince(filePath, sinceTime)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Couldn't get revisions of %s: %s\n", filePath, err)
			os.Exit(1)
		}

		bar := progressbar.NewOptions(
			len(hashes),
			progressbar.OptionSetRenderBlankState(true),
			progressbar.OptionSetWriter(os.Stderr))

		progress := make(chan struct{}, len(hashes))
		var trend measurements.Trend
		go func() {
			trend, err = measurements.GetTrend(git, hashes, filePath, progress)
		}()

		for range progress {
			bar.Add(1)
		}

		if err != nil {
			fmt.Fprintf(os.Stderr, err.Error())
			os.Exit(1)
		}

		sort.Sort(trend)
		json, err := json.Marshal(trend)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Couldn't marshal result to JSON: %s\n", err.Error())
			os.Exit(1)
		}
		fmt.Println(string(json))
	},
}

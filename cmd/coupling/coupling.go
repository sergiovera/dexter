package coupling

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/sergiovera/dexter/pkg/measurements"
	"gitlab.com/sergiovera/dexter/pkg/vcs/git"

	"github.com/schollz/progressbar"
	"github.com/spf13/cobra"
)

var since string
var minimumRevisions int
var minimumPercentage int

func init() {
	Cmd.Flags().StringVarP(&since, "since", "s", "1970-01-01", "Include only commits since this date")
	Cmd.Flags().IntVarP(&minimumRevisions, "revs", "r", 20, "Minimum number of revisions to take into account")
	Cmd.Flags().IntVarP(&minimumPercentage, "percentage", "p", 30, "Minimum number of coupling percentage to take into account")
}

var Cmd = &cobra.Command{
	Use:   "coupling [repository path]",
	Short: "Do a change coupling analysis",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		var coupling measurements.CouplingStruct
		var err error

		path := args[0]
		sinceTime, err := time.Parse("2006-01-02", since)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Passed date not valid, format must be YYYY-MM-DD\n")
			os.Exit(1)
		}

		git := git.New(path)
		revisions, err := git.CommitsSince(sinceTime)
		if err != nil {
			log.Fatalf("Error extracting commits information: %s", err)
		}
		bar := progressbar.NewOptions(
			len(revisions),
			progressbar.OptionSetRenderBlankState(true),
			progressbar.OptionSetWriter(os.Stderr))

		progress := make(chan struct{}, len(revisions))
		go func() {
			coupling, err = measurements.Coupling(git, revisions, minimumRevisions, float64(minimumPercentage), progress)
		}()

		for range progress {
			bar.Add(1)
		}
		if err != nil {
			log.Fatalf("Error extracting coupling information: %s", err)
		}
		if jsonString, err := json.MarshalIndent(coupling, "", "    "); err == nil {
			fmt.Println(string(jsonString))
		} else {
			log.Printf("%s\n", err)
		}
	},
}
